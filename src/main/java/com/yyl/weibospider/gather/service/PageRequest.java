package com.yyl.weibospider.gather.service;

public class PageRequest {
	
	private int pageNum;
	private int pageCount;
	
	
	public PageRequest(int pageNum, int pageCount) {
		super();
		this.pageNum = pageNum;
		this.pageCount = pageCount;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getPageCount() {
		return pageCount;
	}
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
	
	

}
