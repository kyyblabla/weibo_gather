package com.yyl.weibospider.gather.service;

import java.util.List;

import com.yyl.weibospider.gather.domain.WeiboBean;

public interface SpiderServer {
		
	public List<WeiboBean> get();
	
	
}
