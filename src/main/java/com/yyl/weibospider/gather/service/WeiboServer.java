package com.yyl.weibospider.gather.service;

import javax.annotation.Resource;

import com.yyl.weibospider.gather.dao.WeiboDao;
import com.yyl.weibospider.gather.domain.WeiboBean;

public class WeiboServer {

	@Resource
	private WeiboDao weiboDao;

	public WeiboBean save(WeiboBean entity) {

		WeiboBean ow = entity.getoWeibo();

		WeiboBean findWeibo = findByWeiboID(entity.getWeiboID());

		if (findWeibo != null) {
			entity.setoWeiboID(findWeibo.getoWeiboID());
		}

		weiboDao.save(entity);

		if (ow != null) {
			save(ow);
		}

		return entity;

	}

	public WeiboBean findByWeiboID(String id) {

		return weiboDao.findByWeiboID(id);

	}

}
