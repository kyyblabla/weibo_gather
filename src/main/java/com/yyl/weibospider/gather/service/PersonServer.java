package  com.yyl.weibospider.gather.service;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.yyl.weibospider.gather.dao.PersonDao;
import com.yyl.weibospider.gather.domain.PersonBean;

 
public class PersonServer {

	 
	private PersonDao personDao;

	public List<PersonBean> getSomePersonByLastSpiderTime(int status, int n) {

		List<PersonBean> byLastSpiderTime = personDao.getByLastSpiderTime(
				status, new PageRequest(0, n));
		return byLastSpiderTime;

	}

	public List<PersonBean> getSomePersonByLastSpiderTimeTpSpider(int n) {

		List<PersonBean> byLastSpiderTime = personDao.getByLastSpiderTime(
				PersonBean.STU_WAIT_SPIDER, new PageRequest(0, n));

		for (PersonBean personBean : byLastSpiderTime) {
			personBean.setStatus(PersonBean.STU_IN_SPIDER);
		}

		return personDao.save(byLastSpiderTime);

	}

	public List<PersonBean> getSomePersonByLastWeiboUpdateTimeToSpiser(int n) {

		List<PersonBean> byLastWeiboUpdateTime = personDao
				.getByLastWeiboUpdateTime(PersonBean.STU_WAIT_SPIDER,
						new PageRequest(0, n));

		for (PersonBean personBean : byLastWeiboUpdateTime) {
			personBean.setStatus(PersonBean.STU_IN_SPIDER);
		}

		return personDao.save(byLastWeiboUpdateTime);
	}

	public List<PersonBean> getSomePersonByLastWeiboUpdateTime(int status, int n) {

		List<PersonBean> byLastWeiboUpdateTime = personDao
				.getByLastWeiboUpdateTime(status, new PageRequest(0, n));

		return byLastWeiboUpdateTime;

	}

	public PersonBean save(PersonBean personBean) {
		
		return personDao.save(personBean);

	}

	public PersonBean findByPersonPageId(String id) {

		List<PersonBean> ps = personDao.findByPersonPageId(id);

		return ps.size() > 0 ? ps.get(0) : null;

	}
	
	public int  updatePersonSpiderStatus(int newStatus,int odlStatus){
		
		return personDao.updateStatus(newStatus, odlStatus);
		
	}

}
