package com.yyl.weibospider.gather.dao;

import com.yyl.weibospider.gather.domain.WeiboBean;

public interface WeiboDao {

	void save(WeiboBean entity);
	
	void update(WeiboBean entity);
	
	void saveOrUpdate(WeiboBean entity);

	 WeiboBean findByWeiboID(String id);
	
	 WeiboBean findByWeubiDate(String date);
	 
	 WeiboBean findByWeubiKeyword(String keyword);
	 
	 WeiboBean findByBetweenDate(String t1,String t2);
	 
	 int findTotalNumber(String t1);
	 
	 int findNegativeNumber(String t1);
	 

}
