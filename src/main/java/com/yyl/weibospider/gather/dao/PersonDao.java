package com.yyl.weibospider.gather.dao;

import java.util.List;

import com.yyl.weibospider.gather.domain.PersonBean;
import com.yyl.weibospider.gather.service.PageRequest;

public interface PersonDao {

	List<PersonBean> save(List<PersonBean> byLastSpiderTime);

	PersonBean save(PersonBean personBean);

	int updateStatus(int newStatus, int odlStatus);

	List<PersonBean> findByPersonPageId(String id);

	List<PersonBean> getByLastSpiderTime(int status, PageRequest pageRequest);

	List<PersonBean> getByLastWeiboUpdateTime(int stuWaitSpider,
			PageRequest pageRequest);

}
