package com.yyl.weibospider.gather.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;
import com.yyl.weibospider.gather.dao.WeiboDao;
import com.yyl.weibospider.gather.domain.WeiboBean;
import com.yyl.weibospider.gather.util.JbdcUtils;

public class WeiboDaoimpl implements WeiboDao {

	@Override
	public void save(WeiboBean entity) {

		try {
			Connection con = JbdcUtils.getConnection();
			String sql = "INSERT INTO t_sinaweibo (DATECREATED,commentNum, forwardNum, foward, likeNum, oWeiboID, uID, weiboContext, weiboDev, weiboID, weiboTime) VALUES (now(),?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement sta = (PreparedStatement) con
					.prepareStatement(sql);
			sta.setInt(1, entity.getCommentNum());
			sta.setInt(2, entity.getForwardNum());
			sta.setInt(3, entity.getFoward());
			sta.setInt(4, entity.getLikeNum());
			sta.setString(5, entity.getoWeiboID());
			sta.setString(6, entity.getuID());
			sta.setString(7, entity.getWeiboContext());
			sta.setString(8, entity.getWeiboDev());
			sta.setString(9, entity.getWeiboID());
			sta.setString(10, entity.getWeiboTime());

			sta.executeUpdate();

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Override
	public void update(WeiboBean entity) {

		try {
			Connection con = JbdcUtils.getConnection();
			String sql = "update t_sinaweibo set commentNum=?,forwardNum=?, likeNum=? where weiboID=?";
			PreparedStatement sta = (PreparedStatement) con
					.prepareStatement(sql);
			sta.setInt(1, entity.getCommentNum());
			sta.setInt(2, entity.getForwardNum());
			sta.setInt(3, entity.getLikeNum());
			sta.setString(4, entity.getWeiboID());

			sta.executeUpdate();

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Override
	public void saveOrUpdate(WeiboBean entity) {

		WeiboBean findByWeiboID = findByWeiboID(entity.getWeiboID());
		
		if (findByWeiboID != null) {

			update(entity);
			
		} else {
			save(entity);
		}

	}

	@Override
	public WeiboBean findByWeiboID(String id) {

		try {
			Connection con = JbdcUtils.getConnection();
			Statement sta = con.createStatement();
			ResultSet rs = sta
					.executeQuery("select weiboID,uID,weiboContext,weiboDev,weiboTime from t_sinaweibo where weiboID='"
							+ id + "'");
			if (rs.next()) {
				String weiboID = rs.getString(1);
				String uID = rs.getString(2);
				String weiboContext = rs.getString(3);
				String weiboDev = rs.getString(4);
				String weiboTime = rs.getString(5);

				return new WeiboBean(weiboID, uID, weiboContext, weiboDev,
						weiboTime);
			}

		} catch (Exception e) {

			e.printStackTrace();
		}

		return null;

	}

	@Override
	public WeiboBean findByWeubiDate(String date) {

		try {
			Connection con = JbdcUtils.getConnection();
			Statement sta = con.createStatement();
			ResultSet rs = sta
					.executeQuery("select weiboID,uID,weiboContext,weiboDev,weiboTime from t_sinaweibo where weiboTime like '%"
							+ date + "%'");
			while (rs.next()) {
				String weiboid = rs.getString(1);
				String uid = rs.getString(2);
				String context = rs.getString(3);
				String dev = rs.getString(4);
				String time = rs.getString(5);

				return new WeiboBean(weiboid, uid, context, dev, time);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public WeiboBean findByWeubiKeyword(String keyword) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WeiboBean findByBetweenDate(String t1, String t2) {

		try {
			Connection con = JbdcUtils.getConnection();
			Statement sta = con.createStatement();
			ResultSet rs = sta
					.executeQuery("select weiboID,uID,weiboContext,weiboDev,weiboTime from t_sinaweibo where weiboTime like between"
							+ t1 + "and" + t2);
			while (rs.next()) {
				String weiboid = rs.getString(1);
				String uid = rs.getString(2);
				String context = rs.getString(3);
				String dev = rs.getString(4);
				String time = rs.getString(5);

				return new WeiboBean(weiboid, uid, context, dev, time);
			}

		} catch (Exception e) {

			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int findTotalNumber(String t1) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int findNegativeNumber(String t1) {
		// TODO Auto-generated method stub
		return 0;
	}

}
