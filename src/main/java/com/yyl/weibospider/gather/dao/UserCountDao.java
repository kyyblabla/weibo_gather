package com.yyl.weibospider.gather.dao;

import com.yyl.weibospider.gather.domain.UserCountBean;

public interface UserCountDao {

	public UserCountBean getUserCount();

}
