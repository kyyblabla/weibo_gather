package com.yyl.weibospider.gather.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import com.yyl.weibospider.gather.domain.WeiboTaskBean;

 
public class WeiboTaskDataQueue implements GeneraDataQueue<WeiboTaskBean> {

	private int DEFAUTL_QUEUE_SIZE = 100;
	private static BlockingQueue<WeiboTaskBean> dataQueue;

	public WeiboTaskDataQueue(int size) {

		dataQueue = new LinkedBlockingQueue<WeiboTaskBean>(size);

	}

	public WeiboTaskDataQueue() {

		System.out.println("ssdsdsdsdsds");
		dataQueue = new LinkedBlockingQueue<WeiboTaskBean>(DEFAUTL_QUEUE_SIZE);
	}

	public List<WeiboTaskBean> getAll() {

		List<WeiboTaskBean> list = new ArrayList<WeiboTaskBean>();
		dataQueue.drainTo(list);
		return list;

	}

	public WeiboTaskBean poll() {

		WeiboTaskBean w;
		try {
			w = dataQueue.poll(10, TimeUnit.SECONDS);
			return w;
		} catch (Exception e) {

			e.printStackTrace();
		}

		return null;

	}

	public void add(WeiboTaskBean entry) {

		try {
			dataQueue.put(entry);
		} catch (Exception e) {
		}

	}

	public void add(List<WeiboTaskBean> entrys) {

		for (WeiboTaskBean weiboTaskBean : entrys) {
			add(weiboTaskBean);
		}
	}

	public boolean isEmpty() {

		return dataQueue.isEmpty();
	}

}
