package com.yyl.weibospider.gather.util;

import java.util.List;

/**
 * 通用数据队列,它是线程安全的
 * 
 * @author ky
 * 
 * @param <T>
 */
public interface GeneraDataQueue<T> {

	/**
	 * 获取队列中所有数据
	 * 
	 * @return
	 */
	public List<T> getAll();

	/**
	 * 获取对头数据
	 * 
	 * @return
	 */
	public T poll();

	/**
	 * 增加数据
	 * 
	 * @param entrys
	 */
	public void add(T entrys);

	/**
	 * 增加数据
	 * 
	 * @param entrys
	 */
	public void add(List<T> entrys);

	public boolean isEmpty();

}
