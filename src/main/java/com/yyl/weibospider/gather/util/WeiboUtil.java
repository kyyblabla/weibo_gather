/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yyl.weibospider.gather.util;

import java.net.URLEncoder;

import net.sf.json.JSONObject;

/**
 * 
 * @author ky
 */
public class WeiboUtil {

	/**
	 * unicode 解码
	 * 
	 * @param s
	 * @return
	 */
	public static String fromEncodedUnicode(String s) {

		char[] in = s.toCharArray();

		int off = 0;
		int len = in.length;

		char aChar;
		char[] out = new char[len]; // 只短不长

		int outLen = 0;

		int end = off + len;

		while (off < end) {

			aChar = in[off++];

			if (aChar == '\\') {

				aChar = in[off++];

				if (aChar == 'u') {

					// Read the xxxx

					int value = 0;

					for (int i = 0; i < 4; i++) {

						aChar = in[off++];

						switch (aChar) {

						case '0':

						case '1':

						case '2':

						case '3':

						case '4':

						case '5':

						case '6':

						case '7':

						case '8':

						case '9':

							value = (value << 4) + aChar - '0';

							break;

						case 'a':

						case 'b':

						case 'c':

						case 'd':

						case 'e':

						case 'f':

							value = (value << 4) + 10 + aChar - 'a';

							break;

						case 'A':

						case 'B':

						case 'C':

						case 'D':

						case 'E':

						case 'F':

							value = (value << 4) + 10 + aChar - 'A';

							break;

						default:

							throw new IllegalArgumentException(
									"Malformed \\uxxxx encoding.");

						}

					}

					out[outLen++] = (char) value;

				} else {

					if (aChar == 't') {

						aChar = '\t';

					} else if (aChar == 'r') {

						aChar = '\r';

					} else if (aChar == 'n') {

						aChar = '\n';

					} else if (aChar == 'f') {

						aChar = '\f';

					}

					out[outLen++] = aChar;

				}

			} else {

				out[outLen++] = (char) aChar;

			}

		}

		return new String(out, 0, outLen);

	}

	private static char[] base64EncodeChars = new char[] { 'A', 'B', 'C', 'D',
			'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
			'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
			'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
			'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3',
			'4', '5', '6', '7', '8', '9', '+', '/' };

	/**
	 * base64 编码
	 * 
	 * @param text
	 * @return
	 */
	public static String encodeBase64(String text) {

		StringBuffer sb = new StringBuffer();

		byte[] data;
		try {
			data = URLEncoder.encode(text, "UTF-8").getBytes();
			int len = data.length;
			int i = 0;
			int b1, b2, b3;

			while (i < len) {
				b1 = data[i++] & 0xff;
				if (i == len) {
					sb.append(base64EncodeChars[b1 >>> 2]);
					sb.append(base64EncodeChars[(b1 & 0x3) << 4]);
					sb.append("==");
					break;
				}
				b2 = data[i++] & 0xff;
				if (i == len) {
					sb.append(base64EncodeChars[b1 >>> 2]);
					sb.append(base64EncodeChars[((b1 & 0x03) << 4)
							| ((b2 & 0xf0) >>> 4)]);
					sb.append(base64EncodeChars[(b2 & 0x0f) << 2]);
					sb.append("=");
					break;
				}
				b3 = data[i++] & 0xff;
				sb.append(base64EncodeChars[b1 >>> 2]);
				sb.append(base64EncodeChars[((b1 & 0x03) << 4)
						| ((b2 & 0xf0) >>> 4)]);
				sb.append(base64EncodeChars[((b2 & 0x0f) << 2)
						| ((b3 & 0xc0) >>> 6)]);
				sb.append(base64EncodeChars[b3 & 0x3f]);
			}

		} catch (Exception e) {

			e.printStackTrace();
		}

		return sb.toString();
	}

	/**
	 * 将表示微博信息字符串转化为json对象
	 * 
	 * @param text
	 * @return
	 */
	public static String jsonTransform(String text) {

		try {

			JSONObject jsonObject = JSONObject.fromObject(text);

			return jsonObject.get("data").toString();

		} catch (Exception e) {
		}

		return null;

	}

	public static int stringToInt(String str) {

		try {

			return Integer.parseInt(str);

		} catch (Exception e) {
			// TODO: handle exception
		}

		return 0;

	}

	public static void main(String[] args) throws Exception {

		String aa = "{\"data\":\"reiz\"}";

		System.out.println(jsonTransform(aa));

	}
}
