/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yyl.weibospider.gather.domain;

public class WeiboBean {

	public WeiboBean() {
	}

	public WeiboBean(String weiboID, String uID, String weiboContext,
			String weiboDev, String weiboTime) {
		super();
		this.weiboID = weiboID;
		this.uID = uID;
		this.weiboContext = weiboContext;
		this.weiboDev = weiboDev;
		this.weiboTime = weiboTime;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 微博id
	 */
	private String weiboID;
	/**
	 * 用户id
	 */
	private String uID;
	/**
	 * 微博内容
	 */
	private String weiboContext;
	/**
	 * 微博发布设备
	 */
	private String weiboDev;
	/**
	 * 微博发布时间
	 */
	private String weiboTime;

	/**
	 * 转发的微博
	 */
	private WeiboBean oWeibo = null;

	/**
	 * 源微博id
	 */
	private String oWeiboID = null;

	/**
	 * 是否转发自别人的微博
	 */
	private Integer foward = 0;

	/**
	 * 赞的数目
	 */
	private Integer likeNum;

	/**
	 * 抓发数目
	 */
	private Integer forwardNum;

	/**
	 * 评论数目
	 */
	private Integer commentNum;

	public String getuID() {
		return uID;
	}

	public void setuID(String uID) {
		this.uID = uID;
	}

	public String getoWeiboID() {
		return oWeiboID;
	}

	public void setoWeiboID(String oWeiboID) {
		this.oWeiboID = oWeiboID;
	}

	public String getWeiboContext() {
		return weiboContext;
	}

	public void setWeiboContext(String weiboContext) {
		this.weiboContext = weiboContext;
	}

	public String getWeiboDev() {
		return weiboDev;
	}

	public void setWeiboDev(String weiboDev) {
		this.weiboDev = weiboDev;
	}

	public String getWeiboTime() {
		return weiboTime;
	}

	public void setWeiboTime(String weiboTime) {
		this.weiboTime = weiboTime;
	}

	public WeiboBean getoWeibo() {
		return oWeibo;
	}

	public void setoWeibo(WeiboBean oWeibo) {

		if (oWeibo != null) {
			this.oWeiboID = oWeibo.getWeiboID();
		}

		this.oWeibo = oWeibo;
	}

	public Integer getFoward() {
		return foward;
	}

	public void setFoward(Integer foward) {
		this.foward = foward;
	}

	public Integer getLikeNum() {
		return likeNum;
	}

	public void setLikeNum(Integer likeNum) {
		this.likeNum = likeNum;
	}

	public Integer getForwardNum() {
		return forwardNum;
	}

	public void setForwardNum(Integer forwardNum) {
		this.forwardNum = forwardNum;
	}

	public Integer getCommentNum() {
		return commentNum;
	}

	public void setCommentNum(Integer commentNum) {
		this.commentNum = commentNum;
	}

	public String getWeiboID() {
		return weiboID;
	}

	public void setWeiboID(String weiboID) {
		this.weiboID = weiboID;
	}

 
	

}
