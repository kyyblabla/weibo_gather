/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yyl.weibospider.gather.domain;

/**
 * 用户账号，抓取过程中要适时切换账号，防止新浪屏蔽
 * 
 * @author ky
 */
public class UserCountBean {
    
    /**
     * 用户名
     */
    private String userName;
    
    /**
     * 密码
     */
    private String userPassword;

    public UserCountBean() {
    }
    
    public UserCountBean(String userName, String userPassword) {
        this.userName = userName;
        this.userPassword = userPassword;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
    
    
}
