/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yyl.weibospider.gather.domain;

/**
 * 
 * @author qiuniao
 */
public class WeiboTaskBean {

	/**
	 * 停止规则
	 */
	public enum StopReson {
		// 时间 id 页数
		StopByTime, StopByID, StopByPage, NoStop
	};

	private PersonBean person;
	// private String userId;
	private int currPage = 1;
	private StopReson stopReson = StopReson.NoStop;
	private Object stopValue = null;
	private boolean taskOver = false;

	public boolean isTaskOver() {
		return taskOver;
	}

	public void setTaskOver(boolean taskOver) {
		this.taskOver = taskOver;
	}

	// public String getUserId() {
	// return userId;
	// }
	//
	// public void setUserId(String userId) {
	// this.userId = userId;
	// }

	public int getCurrPage() {
		return currPage;
	}

	public void setCurrPage(int currPage) {
		this.currPage = currPage;
	}

	public StopReson getStopReson() {
		return stopReson;
	}

	public void setStopReson(StopReson stopReson) {
		this.stopReson = stopReson;
	}

	public Object getStopValue() {
		return stopValue;
	}

	public void setStopValue(Object stopValue) {
		this.stopValue = stopValue;
	}

	public PersonBean getPerson() {
		return person;
	}

	public void setPerson(PersonBean person) {
		this.person = person;
	}

}
