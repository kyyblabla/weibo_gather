/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yyl.weibospider.gather.domain;

import java.util.Date;
 
public class PersonBean   {

	public static final int STU_WAIT_SPIDER = 0;
	public static final int STU_IN_SPIDER = 1;
	public static final int STU_BEEN_SPIDER = 2;

	private static final long serialVersionUID = 1L;
	// /**
	// * 用户id
	// */
	// @Column(length = 50, unique = true, nullable = false)
	// private String personId;

	/**
	 * page_id 用于ajax 获取微博
	 */
	private String personPageId;
	
	/**
	 * page_id 用于ajax 获取微博
	 */
	private String personId;
	
	/**
	 * 上次微博更新时间
	 */
	private Date lastWeiboUpdateTime;

	/**
	 * 上次更新的微博id
	 */
	private String lastWeiboId;

	private Date lastSpiderTime;

	/**
	 * 人物状态 0.待抓取，1.正在抓取，3，已经抓取
	 */
	private Integer status = 0;

	/**
	 * 人物被抓取的次数
	 */
	private Integer spiderTimes = 0;

	public Integer getSpiderTimes() {
		return spiderTimes;
	}

	public void setSpiderTimes(Integer spiderTimes) {
		this.spiderTimes = spiderTimes;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getLastWeiboUpdateTime() {
		return lastWeiboUpdateTime;
	}

	public void setLastWeiboUpdateTime(Date lastWeiboUpdateTime) {
		this.lastWeiboUpdateTime = lastWeiboUpdateTime;
	}

	public Date getLastSpiderTime() {
		return lastSpiderTime;
	}

	public void setLastSpiderTime(Date lastSpiderTime) {
		this.lastSpiderTime = lastSpiderTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	// public String getPersonId() {
	// return personId;
	// }
	//
	// public void setPersonId(String personId) {
	// this.personId = personId;
	// }

	public String getPersonPageId() {
		return personPageId;
	}

	public void setPersonPageId(String personPageId) {
		this.personPageId = personPageId;
	}

	public String getLastWeiboId() {
		return lastWeiboId;
	}

	public void setLastWeiboId(String lastWeiboId) {
		this.lastWeiboId = lastWeiboId;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

}
