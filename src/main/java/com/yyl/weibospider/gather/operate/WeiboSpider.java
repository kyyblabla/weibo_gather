package com.yyl.weibospider.gather.operate;

import java.util.Date;
import java.util.List;

import com.yyl.weibospider.gather.domain.WeiboBean;

/**
 * 用于微博数据的具体获取，提供较为方便的接口来获取微博，如根据id获取，根据时间获取
 * 
 * @author ky
 * 
 */
public interface WeiboSpider {

	public boolean login(String u, String p);

	/**
	 * 获取用户某页的微博
	 * 
	 * @param userId
	 * @param page
	 * @return
	 */
	public List<WeiboBean> getByPage(String userId, int page);
	
	/**
	 *  获取用户前n条微博
	 * @param userId
	 * @param limit 
	 * @return
	 */
	public List<WeiboBean> getSome(String userId, int limit);

	/**
	 * 获取某个时间之后的微博
	 * 
	 * @param userId
	 * @param timeAfter
	 * @param limit
	 *            数目限制，0表示不限制
	 * @return
	 */
	public List<WeiboBean> getByTimeAfter(String userId, Date timeAfter,
			int limit);

}
