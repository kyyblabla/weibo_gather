/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yyl.weibospider.gather.operate;

 
import java.util.List;

import com.yyl.weibospider.gather.domain.WeiboBean;

 
/**
 * 根据html内容解析出微博数据
 * 
 * @author ky
 */
public interface WeiboContextParse {
    
    /**
     * 解析文本得到微博信息
     * @param text 
     * @return
     * @throws Exception 
     */
    public List<WeiboBean> parse(String text);
    	
    /**
     *  根据页面内容解析出用户数据
     * @param text
     * @return
     */
    public String parseUserPageId(String text);
    
    
}
