package com.yyl.weibospider.gather.operate.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.yyl.weibospider.gather.domain.WeiboBean;
import com.yyl.weibospider.gather.operate.WeiboContextGet;
import com.yyl.weibospider.gather.operate.WeiboContextParse;
import com.yyl.weibospider.gather.operate.WeiboSpider;

public class WeiboSpiderImpl implements WeiboSpider {

	private WeiboContextGet weiboContextGet;
	private WeiboContextParse weiboContextParse;

	public WeiboSpiderImpl() {

		weiboContextGet = new WeiboContextGetImpl();
		weiboContextParse = new WeiboContextParseImpl();

	}

	public List<WeiboBean> getByPage(String userId, int page) {

		List<WeiboBean> weibos = new ArrayList<WeiboBean>();

		// 依次获取一页的三部分微博
		for (int i = 1; i < 4; i++) {
			List<WeiboBean> parse = getByUserIdAndPart(userId, page, i);
			if (parse.isEmpty()) {
				break;
			}
			weibos.addAll(parse);
		}
		return weibos;
	}

	private List<WeiboBean> getByUserIdAndPart(String userId, int page, int part) {

		String weiboPageByUserId;
		// 获取第一部分
		if (part == 1) {

			weiboPageByUserId = weiboContextGet.getWeiboPageByUserId(userId, 0,
					page, 1);
		} else if (part == 2) {
			// 获取第二部分
			weiboPageByUserId = weiboContextGet.getWeiboPageByUserId(userId,
					page, page, 0);
		} else {
			// 获取第三部分
			weiboPageByUserId = weiboContextGet.getWeiboPageByUserId(userId,
					page, page, 1);
		}

		List<WeiboBean> parse = weiboContextParse.parse(weiboPageByUserId);

		return parse;

	}

	public List<WeiboBean> getByTimeAfter(String userId, Date timeAfter,
			int limit) {

		List<WeiboBean> weibos = new ArrayList<WeiboBean>();

		int page = 1;
		boolean isAfter = false;
		while (true) { // while start

			// 依次获取一页的三部分微博
			for (int i = 1; i < 4; i++) { // for statr

				// 获取一部分微博
				List<WeiboBean> parse = getByUserIdAndPart(userId, page, i);

				// 为空说明数据已经获取完毕
				if (parse.isEmpty()) {
					return weibos;
				}

				// 遍历微博，对微博的发布时间进行判断
				for (WeiboBean weiboBean : parse) { // foeach start

					// 格式化时间
					SimpleDateFormat sdf = new SimpleDateFormat(
							"yyyy-MM-dd hh:mm");
					Date date1;
					try {
						date1 = sdf.parse(weiboBean.getWeiboTime());
						isAfter = date1.after(timeAfter);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					// 如果当前微博在限定时间之后，这添加微博
					if (isAfter) {

						// 对当前数据容量进行判断，看数据是否超标
						if (limit > 0 && weibos.size() >= limit) {
							return weibos;
						}
						weibos.add(weiboBean);

					} else {
						return weibos;
					}
				} // foreach end

			} // for end
			page++;
		} // while end

	}

	public boolean login(String u, String p) {

		return weiboContextGet.login(u, p);

	}

	public List<WeiboBean> getSome(String userId, int limit) {

		List<WeiboBean> weibos = new ArrayList<WeiboBean>();

		int page = 1;

		while (true) { // while start

			// 依次获取一页的三部分微博
			for (int i = 1; i < 4; i++) { // for statr

				// 获取一部分微博
				List<WeiboBean> parse = getByUserIdAndPart(userId, page, i);

				// 为空说明数据已经获取完毕
				if (parse.isEmpty()) {
					return weibos;
				}

				// 遍历微博，对微博的发布时间进行判断
				for (WeiboBean weiboBean : parse) { // foeach start

					// 对当前数据容量进行判断，看数据是否超标
					if (limit > 0 && weibos.size() >= limit) {
						return weibos;
					}
					weibos.add(weiboBean);

				} // foreach end

			} // for end
			page++;
		} // while end

	}

}
