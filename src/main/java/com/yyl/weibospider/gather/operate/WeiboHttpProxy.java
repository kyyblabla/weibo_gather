/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yyl.weibospider.gather.operate;

/**
 *
 * @author qx
 */
public interface WeiboHttpProxy {

    /**
     * 代理对象
     */
    public class Proxy {

        /**
         * 代理地址
         */
        private String proxyName;
        /**
         * 搭理端口
         */
        private int proxyPort;

        public Proxy(String proxyName, int proxyPort) {
            this.proxyName = proxyName;
            this.proxyPort = proxyPort;
        }

        /**
         * 代理地址
         */
        public String getProxyName() {
            return proxyName;
        }

        public void setProxyName(String proxyName) {
            this.proxyName = proxyName;
        }

        public int getProxyPort() {
            return proxyPort;
        }

        public void setProxyPort(int proxyPort) {
            this.proxyPort = proxyPort;
        }
    }

    /**
     * 获取代理
     *
     * @return
     */
    public Proxy getProxy();
}
