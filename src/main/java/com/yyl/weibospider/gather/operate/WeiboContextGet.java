/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yyl.weibospider.gather.operate;

import java.util.Date;

/**
 * 微博页面内容获取相关类，这里获取到的为纯html代码
 * 
 * @author ky
 * 
 */
public interface WeiboContextGet {

	/**
	 * 登陆
	 * 
	 * @param u
	 *            用户名
	 * @param p
	 *            密码
	 * @return
	 * @throws Exception
	 *             登陆失败
	 */
	public boolean login(String u, String p);

	/**
	 * 检查并设置代理服务器
	 */
	public void checkProxy();

	/**
	 * 根据用户名获取微博的html文本
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public String getWeiboPageByUserId(String userId);

	/**
	 * 获取用户微博列表的某页某部分 微博列表请求是分页的，每页分为三部分，每部分15（默认）条数据
	 * 
	 * @param userId
	 *            用户id
	 * @param preNum
	 *            非ajax请求预加载的页数，这个参数主要用来“骗”服务器
	 * @param pageNum
	 *            页数 0 - ∞
	 * @param partNum
	 *            块数 0-3
	 * @return
	 * @throws Exception
	 */
	public String getWeiboPageByUserId(String userId, int preNum, int pageNum,
			int partNum);
 
}
