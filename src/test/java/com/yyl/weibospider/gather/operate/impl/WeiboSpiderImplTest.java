package com.yyl.weibospider.gather.operate.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.yyl.weibospider.gather.domain.WeiboBean;

import junit.framework.TestCase;

public class WeiboSpiderImplTest extends TestCase {

	WeiboSpiderImpl w = new WeiboSpiderImpl();

	@Test
	public void testGetByPage() {

		boolean l = w.login("kyyblabla@163.com", "519519519");
		Assert.assertTrue(l);

		List<WeiboBean> list = w.getByPage("1005051164552632", 1);

		for (WeiboBean weiboBean : list) {
			System.out.println(weiboBean.getWeiboID());
			System.out.println(weiboBean.getWeiboContext());
			System.out.println("------------------");
		}

		System.out.println("=================");
	}

	public void testGetByTimeAfter() throws Exception {

		boolean l = w.login("kyyblabla@163.com", "519519519");
		Assert.assertTrue(l);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");

		Date d = sdf.parse("2014-05-01 00:00");

		List<WeiboBean> list = w.getByTimeAfter("1005051164552632", d, 0);

		int i = 1;
		for (WeiboBean weiboBean : list) {

			System.out.println(i++);
			System.out.println(weiboBean.getWeiboID());
			System.out.println(weiboBean.getWeiboTime());
			System.out.println(weiboBean.getWeiboContext());
			System.out.println("------------------");
		}

		System.out.println("=================");
	}

	public void testLogin() {

		boolean l = w.login("kyyblabla@163.com", "519519519");
		System.out.println(l);

	}

	public void testGetSome() {

		boolean l = w.login("kyyblabla@163.com", "519519519");
		Assert.assertTrue(l);

		List<WeiboBean> list = w.getSome("1005051164552632", 20);

		int i = 1;
		for (WeiboBean weiboBean : list) {

			System.out.println(i++);
			System.out.println(weiboBean.getWeiboID());
			System.out.println(weiboBean.getWeiboTime());
			System.out.println(weiboBean.getWeiboContext());
			System.out.println("------------------");
		}

		System.out.println("=================");

	}

}
